#!make

aws.ssm_path = aws ssm get-parameters-by-path --path $(1)

aws.backend:
	$(eval AWS_BACKEND = $(shell $(call aws.ssm_path,$(AWS_BACKEND_BASE_PATH))))
	$(shell echo $(shell $(call jq.format, $(AWS_BACKEND), .Parameters | map({(.Name|split("/")|.[-1]):.Value}) | add ) > backend.auto.tf.json))

# export backend to JSON auto TF file	
backend.auto.tf.json:
	@$(MAKE) -s aws.backend

#whoami: @ Affiche la session AWS API courante
aws.whoami:
	aws sts get-caller-identity

#session: @ Connexion SSM sur une instance ${TARGET}
aws.session:
	aws ssm start-session --target ${TARGET}

define aws.clear_session
	$(eval export AWS_ACCESS_KEY_ID=)
	$(eval export AWS_SECRET_ACCESS_KEY=)
	$(eval export AWS_SESSION_TOKEN=)
	$(eval export AWS_SECURITY_TOKEN=)
	$(eval export AWS_VAULT=)
endef

aws.cls-%:
	$(call aws.clear_session)
	@$(MAKE) $(shell echo ${MAKECMDGOALS} | awk '{print substr($$0, 9)}' ) MAKEFLAGS="${MAKEFLAGS}"

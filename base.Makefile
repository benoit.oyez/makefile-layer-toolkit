#!make

.DEFAULT_GOAL := help

#help: @ Liste les targets disponibles pour ce projet
help:
	@grep -E '[a-zA-Z\.\-]+:.*?@ .*$$' $(MAKEFILE_LIST) | tr -d '#'  | awk 'BEGIN {FS = ":|(: @ )"}; {printf "\033[36m%-30s\033[0m %s\n", $$2, $$3}'| sort

# Helpers

$(DEFAULT_GENERATED_FILES):
	$(DEFAULT_FILE_GENERATION)

jq.format = jq --null-input '$(1)' | jq '$(2)'
#!make

#svc-X: @ Exécuter une target dans un conteneur interactif du service ${SERVICE} du docker-compose local
svc-%:
	docker-compose run --rm --no-deps ${SERVICE} make $(shell echo ${MAKECMDGOALS} | awk '{print substr($$0, 5)}' )

#compose: @ Exécuter le cluster docker-compose local
compose:
	docker-compose up -d --force-recreate ${SERVICE}
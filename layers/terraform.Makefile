#!make
define tf_exec_action
	$(shell echo $${TF_INIT_MUTED:+"chronic"}) terraform init -reconfigure
	terraform $(1)
endef

tf.action: TFACTION ?= apply
tf.action:
	$(call tf_exec_action,$(TFACTION))

#terrafile: @ Télécharge les dépendances Terraform du projet
TF_VENDOR_BASE ?=
terrafile: 
	cd ${TF_VENDOR_BASE} && terrafile -p ${TERRAFILE_VENDOR_ROOT}

$(TF_VENDOR_BASE)/vendor:
	@$(MAKE) terrafile
